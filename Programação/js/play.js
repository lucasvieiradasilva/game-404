
    var score = 0;
    var batida, colisao,jornais;
    var playState = {

     preload: function () {
        this.velocidade= 100;
	    /*game.load.spritesheet('player2','img/player2.png', 80,80);
	    game.load.spritesheet('colidir','img/colidir.png',72, 79);
        game.load.spritesheet('jornal','img/jornal_34_34.png',34,34);
        game.load.image('carro', 'img/carro.png');
        game.load.image('buraco','img/buraco.png');
        game.load.image('barreira', 'img/Barreira.png');
        game.load.image('vida','img/vida.png');
        game.load.image('arvore', 'img/árvore.png');
        game.load.audio('ping', 'sons/getMoeda.wav');
        game.load.audio('incidental', 'sons/8bit Bossa POR Joth.mp3');
		game.load.audio('dano', 'sons/sfx_exp_shortest_soft8.wav');*/
    },
    //END PRELOAD
      
    create: function () {
        game.physics.startSystem(Phaser.Physics.ARCADE);
        this.inc= game.add.audio("incidental");
	
        //BACKGROUND
        background = game.add.tileSprite(0, 0, this.world.width, this.world.height, 'background');
        //arvores
        this.arvores= game.add.group();
        // GRUPO DE MOEDAS
        this.moedas = game.add.group();
    
        // HOUSES
        this.houses = game.add.group();

        // Jornais
        //this.jornais = game.add.group();
        //game.time.events.loop(4000, this.adiconajornal, this);

        // BUCARO
        this.buracos = game.add.group();
        //game.time.events.loop(6000, this.addObstaculos, this);
        
        //CARROS    
        this.carros= game.add.group();
        //game.time.events.loop(19000, this.carro, this);

        //BARRIERA
        this.barreiras= game.add.group();
        //game.time.events.loop(16000, this.barreira, this);

        // PLAYER
        this.player = game.add.sprite(232, 210, 'player');
        game.physics.enable(this.player, Phaser.Physics.ARCADE);
        this.player.animations.add('pedalar', [1, 2, 3, 4], 10, true);
        this.player.animations.add('esquerda', [29, 30, 31, 32], 10, true);
        this.player.animations.add('direita', [25, 26, 27, 28], 10, true);
        this.player.animations.add('colisao', [17,18,19,20,21], 22, false);
        this.player.animations.add('gameover', [18], 0, true);
		
		batida = game.add.sprite(232,210, 'player');
        game.physics.arcade.enable(batida,Phaser.Physics.ARCADE);
        batida.animations.add('colisao', [17, 18, 19, 20, 21], 22, false);

        // COLUNA DIREITA
        this.tileWidth = this.game.cache.getImage('casa2').width;
        this.tileHeight = this.game.cache.getImage('casa2').height;
        this.platforms = this.game.add.group();
        this.platforms.enableBody = true;
        this.platforms.createMultiple(2, 'casa2');
        this.spacing = 0;
        this.initPlatforms();

        // VIDAS
        var vidasbg = game.add.sprite(0, 0, 'vidas_background');

        /* vidas off */
        this.vidasoff = game.add.group();
        this.vida3 = game.add.sprite(52, 6, 'vida');
        this.vida2 = game.add.sprite(80, 6, 'vida');
        this.vida1 = game.add.sprite(107, 6, 'vida');
    			
        // EVENTOS TEMPORARIOS
        game.time.events.loop(9000, this.addPlatform, this);
        game.time.events.loop(5000, this.adicionarMoeda, this);
        game.time.events.loop(11000, this.colunaEsquerda, this);
        game.time.events.loop(9000, this.addObstaculos, this);
        game.time.events.loop(18000, this.carro, this);
        game.time.events.loop(15000, this.barreira, this);
        
        
        //Timer
        game.time.min= 0;
        //this.txtTimer = game.add.text(game.world.width - 15,15,'TIME: ' + game.time.min,{font:'20px emulogic',fill:'#000'});
        //this.txtTimer.anchor.set(1,0);
        this.timer = game.time.events.loop(1000,function(){
        game.time.min++;
        //this.txtTimer.text = 'TIME: ' + game.time.min;
        },this);
        //Pontuação
        game.total.pont = 0;
        txtscore = game.add.text(game.world.width - 133, 10, 'ESCORE: ' + game.total.pont , {fontSize: '20px',  textAlign: 'center'});
       
    },
/* game.input.onDown.add(removeMusic, this);//elemento dentro da função create
 removeMusic:function() {
    music.destroy();
    game.cache.removeSound('incidental');
},*/
       // ADICIONA OBSTACULOS
       addObstaculos: function () {
        /* OBSTACULO :: TIPO BURACO */
            var randomX = game.rnd.integerInRange(160, 303);
            var buraco = game.add.sprite(randomX, -56, 'buraco');
            this.buracos.add(buraco);
            game.physics.arcade.enable(buraco);
            buraco.body.velocity.y = this.velocidade;
            buraco.checkWorldBounds = true;
            buraco.outOfBoundsKill = true;
        },
       
        carro : function(){
            var randomX= game.rnd.integerInRange(170, 303);
            var carro = game.add.sprite(randomX, -25, 'carro');
            this.carros.add(carro);
            game.physics.arcade.enable(carro);
            carro.body.velocity.y = this.velocidade;
            carro.checkWorldBounds = true;
            carro.outOfBoundsKill = true;
            carro.body.immovable = false;
            carro.body.bounce.y=0.2;
            //Define gravidade do inimigo
            carro.body.gravity.y = 1500;
            //Faz inimigos não fugirem do mundo
            carro.body.collideWorldBounds = false;
        },
        barreira: function(){
            var randomX= game.rnd.integerInRange(210, 303);
            var barreira = game.add.sprite(randomX, -30, 'barreira');
            this.barreiras.add(barreira);
            game.physics.arcade.enable(barreira);
            barreira.body.velocity.y = this.velocidade;
            barreira.checkWorldBounds = true;
            barreira.outOfBoundsKill = true;
          },
        //ADICIONA MOEDAS
        adicionarMoeda: function () {
            var moedaX = game.rnd.integerInRange(160, 303);
            var moeda = game.add.sprite(moedaX, -30, 'moeda');
            this.moedas.add(moeda);
            game.physics.arcade.enable(moeda);
            moeda.body.velocity.y = this.velocidade;
            moeda.animations.add('girar', [0, 1, 2, 3, 4, 5, 6, 7], 16, true);
            moeda.animations.play('girar');
            moeda.checkWorldBounds = true;
            moeda.outOfBoundsKill = true;
        },
        addscore: function () {
            if(game.global.pont>=20){
                 game.global.score++;
                 this.vida4 = game.add.sprite(107, 6, 'vida');			 
                 console.log(game.global.score++);
                } 
        //	console.log(this.vida4.kill());  
        },
        vidas: function(){
            if (game.global.score == 3) {
                this.vida1.kill();
                game.global.score--;
                this.addscore();
                console.log(this.addscore);
            } else if (game.global.score == 2) {
                this.vida2.kill();
                game.global.score--;      
                this.addscore();
            console.log(this.addscore);
            } else if (game.global.score == 1) {
                this.vida3.kill();
                game.state.start('menu');
                //player.animations.play('gameover');    
            } else {
                game.state.start('menu');
            }
         },
        // COLISAO ENTRE PERSONAGEM E RECOMPENSA
        colisaoPlayerMoeda: function (player, moeda) {
            var snd = game.add.audio("ping");
            snd.play();
            moeda.kill();
            // adiciona as atualizações aos escore
            game.total.pont +=10;
            txtscore.text = 'ESCORE: ' + game.total.pont;
            console.log(game.total.pont);
        },
        // COLISAO ENTRE PERSONAGEM E OBSTACULOS
        colisaoPlayerBuraco: function (player, obstaculo) {
            this.dano= game.add.audio("dano");
            this.dano.play();
            batida.animations.play('colisao');
            obstaculo.kill();
            //player.animations.add('batida', [0,1,2,3,4], 1, true);
            //player.animations.play('batida');
            obstaculo.kill();
            this.velocidade-=15;
            console.log(this.velocidade);
            this.vidas();
            console.log(this.vidas);
            this.addscore();
            console.log(this.addscore);
        },
        colisaoPlayerCarro: function (player, carro) {
            this.dano= game.add.audio("dano");
            this.dano.play();
            batida.animations.play('colisao');
            carro.kill();
            //player.animations.add('batida', [0,1,2,3,4], 1, true);
            //player.animations.play('batida');
            carro.kill();
            this.velocidade-=10;
            console.log(this.velocidade);
            this.vidas();
            console.log(this.vidas);
        },
         colisaoPlayerBarreira: function (player, barreira) {
            this.dano= game.add.audio("dano");
            this.dano.play();
            batida.animations.play('colisao');
            barreira.kill();
            //player.animations.add('batida', [0,1,2,3,4], 1, true);
            //player.animations.play('batida');
            //Efeitos de colisão entre obstaculo e personagem
            /*game.physics.arcade.enable(this.player);
            this.player.body.gravity.y=10;
            this.player.body.bounce.y=0.2;
            this.player.body.collideWorldBounds=true;
            game.physics.arcade.collide(this.player,barreira);*/
            barreira.kill();
            this.velocidade-=5;
            console.log(this.velocidade);
            this.vidas();
            console.log(this.vidas);
        },
        /*colisao: function (player, buracos, carros, barreiras){
            batida.animations.play('colisao');
            buraco.kill();
            carro.kill();
            barreira.kill();
            },*/
        //END COLISAO
        /*adiconajornal: function(){
            var jornalX= game.rnd.integerInRange(136, -400);
            var jornal = game.add.sprite(jornalX, -56, 'jornal');
            this.jornais.add(jornal);
            game.physics.arcade.enable(jornal);
            jornal.body.velocity.y = this.velocidade;
            jornal.checkWorldBounds = true;
            jornal.outOfBoundsKill = true;
        },*/
        // COLUNA DIREITA
        addTile: function (x, y) {
            var me = this;
            var tile = me.platforms.getFirstDead();
            //redefini as coordenadas especificas.
            tile.reset(x, y);
            tile.body.velocity.y = this.velocidade;
            tile.body.immovable = true;
            tile.checkWorldBounds = true;
            tile.outOfBoundsKill = true;
        },
    
      
        // END COLUNA DIREITA
        addPlatform: function (y) {
            if (typeof(y) == "undefined") {
                y = -this.tileHeight;
            }
            this.addTile(404, y);
        },
    
        initPlatforms: function () {
            var me = this,
                bottom = me.game.world.height - me.tileHeight,
                top = me.tileHeight;
            for (var y = bottom; y > top - me.tileHeight; y = y - me.spacing) {
                me.addPlatform(y);
            }
        },
        // ADICIONAR OS ELEMENTOS NA COLUNA ESQUERDA
        colunaEsquerda: function () {
            var house = game.add.sprite(136, -400, 'casa2');
            house.scale.setTo(-1, 1);
            this.houses.add(house);
            game.physics.arcade.enable(house);
            house.body.velocity.y = this.velocidade;
            house.checkWorldBounds = true;
            house.outOfBoundsKill = true;
        },
       arvore : function (){
            var arvore = game.add.sprite(136, 400, 'arvore');
            arvore.scale.setTo(-1, 1);
            this.arvores.add(arvore);
            game.physics.arcade.enable(arvore);
            arvore.body.velocity.y = this.velocidade;
            arvore.checkWorldBounds = true;
            arvore.outOfBoundsKill = true
       },
    //END CREATE
    update: function () {
        //BACKGROUND INFINITO
        this.velocidade +=1 * 0.03;
        background.tilePosition.y += this.velocidade / 60;
        console.log(this.velocidade);
        
        //sons
        this.inc.play();

        //CONTROLES DO PERSONAGEM
        if (cursors.left.isDown) {
            if (this.player.x > 155) {
                this.player.x -= 4;
                this.player.animations.play('esquerda');
            }
        }
        else if (cursors.right.isDown) {
            if (this.player.x < 309) {
                this.player.x += 4;
                this.player.animations.play('direita');
            }
        }
        else {
            this.player.animations.play('pedalar');
        }
        //ENCONTROS DE ELEMENTOS
        game.physics.arcade.overlap(this.player, this.buracos, this.colisaoPlayerBuraco, null, this);
        game.physics.arcade.overlap(this.player, this.moedas, this.addscore, this.colisaoPlayerMoeda, null, this);
        game.physics.arcade.overlap(this.player, this.buracos, this.carros, this.barreiras, this.colisao, null, this);
        game.physics.arcade.overlap(this.player, this.carros, this.colisaoPlayerCarro, null, this);
        game.physics.arcade.overlap(this.player, this.barreiras, this.colisaoPlayerBarreira, null, this);
      
    },
    //END UPDATE
 
};
//END PLAYSTATE
