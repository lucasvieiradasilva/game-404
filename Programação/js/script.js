(function () {
    var game = new Phaser.Game(540, 303, Phaser.AUTO, 'container-game', {
        preload: preload,
        create: create,
        update: update
    });

    var casasD, casasE, moedas, buracos, estrada, player, keys, txtScore, score = 0, batida;

    function preload() {

        // CARREGAMENTO DE IMAGENS
        game.load.image('background', 'img/background.png');
        game.load.spritesheet('boneco', 'img/player.png', 37, 78);
        game.load.spritesheet('moeda', 'img/moeda.png', 31, 31);
        game.load.spritesheet('colidir', 'img/colidir.png', 72, 79);
        game.load.image('casa1', 'img/casa1.png', 136, 400);
        game.load.image('casa2', 'img/casa2.png', 136, 400);
        game.load.image('buraco', 'img/buraco.png', 56, 57);

        // CARREGAMENTO DE SONS
        game.load.audio('ping', 'sons/getMoeda.wav');

    }

    function create() {
        this.velocidade = 60;

        // BACKGROUND
        estrada = game.add.tileSprite(0, 0, 540, 303, 'background');

        // MOEDAS
        moedas = game.add.group();
        game.time.events.loop(5000, adicionaMoeda, this);

        // OBSTACULOS
        buracos = game.add.group();
        game.time.events.loop(2900, adicionaBuraco, this);


        // CASAS
        casasD = game.add.group();
        game.time.events.loop(11000, adicionaCasaE, this);
        casasE = game.add.group();
        game.time.events.loop(9500, adicionaCasaD, this);


        // FISICAS DO JOGO
        game.physics.startSystem(Phaser.Physics.ARCADE);
        keys = game.input.keyboard.createCursorKeys();


        // APLICAçÔES SOB O PERSONAGEM
        player = game.add.sprite(252, 200, 'boneco');
        game.physics.arcade.enable(player);
        player.animations.add('pedalar', [0, 1, 2, 3], 10, true);
        player.animations.play('pedalar');

        //batida = game.add.sprite(250,200, 'colidir');
        //game.physics.arcade.enable(batida);
        //batida.animations.add('colisao', [0, 1, 2, 3, 4], 10, false);


        // PONTUAÇÕES
        txtScore = game.add.text(5, 10, 'ESCORE: 0', {fontSize: '20px', fill: '#000'});
    }

    function update() {

        // INTERASOES DO PERSONAGEM
        game.physics.arcade.overlap(player, moedas, coletarMoeda);
        game.physics.arcade.overlap(player, buracos, colisao);

        // BACKGROUND INFINITO
        this.velocidade += 1 * 0.03;
        estrada.tilePosition.y += this.velocidade / 60;
        console.log(this.velocidade);


        // CONTROLES DO PERSONAGEM
        if (keys.left.isDown) {
            if (player.x > 173) {
                player.x -= 6;
            }
        }
        if (keys.right.isDown) {
            if (player.x < 329) {
                player.x += 6;
            }
        }
    }

    function adicionaMoeda() {
        var moedaX = game.rnd.integerInRange(176, 330);
        var moeday = game.rnd.integerInRange(50, 100);
        moeda = moedas.create(moedaX, -30, 'moeda');
        moeda.animations.add('girar', [0, 1, 2, 3, 4, 5, 6, 7], 10, true);
        moeda.animations.play('girar');
        game.physics.arcade.enable(moeda);
        moeda.body.velocity.y = this.velocidade;
    }

    function coletarMoeda(player, moeda) {
        var snd = game.add.audio("ping");
        snd.play();
        moeda.kill();
        score += 10;
        txtScore.text = 'ESCORE: ' + score;
    }

    function adicionaBuraco() {
        var buracoX = game.rnd.integerInRange(178, 306);
        buraco = buracos.create(buracoX, -30, 'buraco');
        game.physics.arcade.enable(buraco);
        buraco.body.velocity.y = this.velocidade;
    }

    function colisao(player) {
        batida.animations.play('colisao');
        buraco.kill();

    }

    function adicionaCasaE() {
        casa1 = casasD.create(0, -400, 'casa1');
        game.physics.arcade.enable(casa1);
        casa1.body.velocity.y = this.velocidade;
    }

    function adicionaCasaD() {
        casa2 = casasE.create(404, -500, 'casa2');
        game.physics.arcade.enable(casa2);
        casa2.body.velocity.y = this.velocidade;
    }
}());

