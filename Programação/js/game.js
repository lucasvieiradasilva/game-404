// Funcao Princpal, cria o Objeto Game do Phaser
var game = new Phaser.Game(540, 303, Phaser.AUTO, 'container-game');


// VARIAVEIS GLOBAIS
game.global = {
    score: 3
};
game.time = {
     min: 0 
};
game.total = {
    pont: 0
};
game.total={
	moedas: 0
}

// Funcoes de Estado
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('play', playState);
game.state.add('menu', menuState);
//game.state.add('end',  endState);

// Estado inicial
game.state.start('boot');