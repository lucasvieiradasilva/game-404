var contador1, segundos, contador2;

document.onkeydown = checkKey;

function checkKey(e) {

    e = e || window.event;

    if (e.keyCode == '38') {
        clearTimer();
    }
    else if (e.keyCode == '40') {
        clearTimer();
    }
    else if (e.keyCode == '37') {
        clearTimer();
    }
    else if (e.keyCode == '39') {
        clearTimer();
    }
    else if (e.keyCode == '32') {
        clearTimer();
    }

}

document.documentElement.addEventListener('keydown', function (e) {
    if ( ( e.keycode || e.which ) == 32) {
        e.preventDefault();
        clearTimer();
    }
}, false);


function contador(segundos,pagina){
    contador1 = setTimeout('redireciona(\''+pagina+'\')', segundos*1000);
    atualiza(segundos);
}


function atualiza(segundos){
    if(segundos>0){
        $("#time").html(segundos);
        segundos = segundos-1;
        contador2 = setTimeout('atualiza(\''+segundos+'\')', 1000);
    }
}

function redireciona(pagina){
    if(pagina != ''){
        window.location = pagina;
    }
}

function clearTimer(){
    clearTimeout(contador1);
    clearTimeout(contador2);
    redireciona("");
    $("#timer-content").animate({opacity:0}, 1000);
}


$(function(){

    var time = $('#time').text();
    contador(time, 'http://www.opovo.com.br/');

})