var loadState = {

    preload: function() {

        game.load.image('pag404', 'img/pagina-nao-encontrada.png');
        game.load.image('background','img/background.png');
        game.load.image('buraco','img/buraco.png');
        game.load.image('play_button','img/play.png');
        game.load.image('vidas_background','img/vidas-background.png');
        game.load.image('vida','img/vida.png');
        game.load.image('carro', 'img/carro.png');
        game.load.image('barreira', 'img/Barreira.png');
        //game.load.spritesheet('player2','img/player2.png',800,150);
        game.load.image('estrelas1', 'img/estrelas1.png');
		game.load.image('estrelas2', 'img/estrelas2.png');
		game.load.image('estrelas3', 'img/estrelas3.png');
        game.load.image('gameover', 'img/SHARE.png');
		game.load.image('rederizar', 'img/botao.png');
        /*temp*/
        game.load.image('casa2','img/casa2.png');
        game.load.image('casaTemp','img/casa2.png');
        game.load.image('arvore', 'img/árvore.png');
        /*temp*/
        game.load.spritesheet('player','img/player2.png', 80, 80);
        //game.load.spritesheet('player2','img/player2.png',465, 640);
		game.load.spritesheet('moeda','img/moeda.png', 31, 31);
		//game.load.spritesheet('colidir','img/colidir.png',72, 79);
        game.load.spritesheet('jornal','img/jornal_voando_44_76.png', 44,76);
        //Carregando de trilha sonora
        game.load.audio('ping', 'sons/getMoeda.wav');
        game.load.audio('incidental', 'sons/8bit Bossa POR Joth.mp3');
        game.load.audio('dano', 'sons/sfx_exp_shortest_soft8.wav');
        game.load.audio('over', 'sons/Game Over 02.mp3');

    },

    create: function() {
        //Background da Caixa
        game.stage.backgroundColor = '#fff';

        game.add.sprite(86, 60, 'pag404');

        cursors = game.input.keyboard.createCursorKeys();
        this.playButton = game.add.button(310, 215, 'play_button', this.startGame, this); 

		// PLAYER
		player = game.add.sprite(232, 210, 'player');
		game.physics.arcade.enable(player);
		player.animations.add('pedalar', [1, 2, 3, 4], 10, true);
        player.animations.play('pedalar');
        player.animations.play('gameover',[1,2,3,4], 10, true);
           },
    update: function() {
        if(cursors.left.isDown){
            game.state.start('play');
        }
        else if(cursors.right.isDown){
            game.state.start('play');
        }
        else if(cursors.up.isDown){
            game.state.start('play');
        }
        else if(cursors.down.isDown){
            game.state.start('play');
        }
     }, 
       startGame: function() {
    
        game.state.start('play');

    }
};