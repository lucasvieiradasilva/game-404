var menuState = {

	preload: function() {

		game.load.audio('over', 'sons/Game Over 02.mp3');
	},

	create: function() {
	 game.stage.backgroundColor = '#D3D3D3';	
	 this.over=game.add.audio("over");
  	 // GAMEOVER
  	 var placarbg = game.add.sprite(0, 0, 'gameover');

 	 /* estrelas off */
     this.estrelasoff = game.add.group();
  	 this.estrelas3 = game.add.sprite(0, 0, 'estrelas3');
  	 this.estrelas2 = game.add.sprite(0,0, 'estrelas2');
   	 this.estrelas1 = game.add.sprite( 0,0,'estrelas1');
	 game.total.moedas=game.total.pont / 10;

	 //this.time_60=number_format(game.time.min, 2, ',', '.');
	 txtscore = game.add.text(160, 150, 'Pontuação: ' +game.total.pont, {fontSize: '30px', fill: '##0066ff', textAlign: 'center'});
     txtmoedas = game.add.text(160, 180, 'Moedas: ' + game.total.moedas, {fontSize: '30px', fill: '##0066ff', textAlign: 'center'});
	 txttime = game.add.text(160, 210, 'Tempo: ' + game.time.min, {fontSize: '30px', fill: '##0066ff', textAlign: 'center'});
	 this.rederizar = game.add.button(250, 250, 'rederizar', this.startGame, this); 
	 },

	update: function (){
	 if (game.total.pont==0){
			this.estrelas3.kill();
			this.estrelas2.kill();
			console.log(game.global.pont);
	 }
	 else if (game.total.pont!=0 && game.total.pont<=100){
			this.estrelas3.kill();
			this.estrelas1.kill();
			console.log(game.global.pont);
			
	 }else if (game.total.pont>100){
			this.estrelas1.kill();
			this.estrelas2.kill();
			console.log(game.global.pont);
	 }
	 txtscore.text = 'Pontuação: ' + game.total.pont;
	 txtmoedas.text ='Moedas: ' + game.total.moedas;
	//tempo_total_do_jogo	 
	if(game.time.min<60){
			game.time.min = (game.time.min * 60)/60;
			txttime.text = 'Tempo: '+ game.time.min + ' Segundos';
	}else if (game.time.min>=60 && game.time.min<120 ){
			//game.time.min = (game.time.min * 60)/60;
			//game.time.min=(game.time.min+60)/100;
			txttime.text = 'Tempo: ' + '1 minuto';	
	}else if (game.time.min>=120  && game.time.min<180  ){
			txttime.text = 'Tempo: ' + '2 minutos';	
	}else if (game.time.min>=180  && game.time.min<240){
			txttime.text = 'Tempo: ' + '3 minutos';	
	}else if (game.time.min>=240  && game.time.min<300){
			txttime.text = 'Tempo: ' + '4 minutos';	
	}else if (game.time.min>=300  && game.time.min<360){
			txttime.text = 'Tempo: ' + '5 minutos';	
	}else{
		 	txttime.text = 'Tempo: ' + 'Mais de 5 minutos';
		 }
		 //som_de_game_over
		 this.over.play();
	 },
	 	
   
	//placar : function(){
		//window.onload = function () { setTimeout('location.reload(http://localhost/Game_404_test2/Programação/game_404.html);', 5000); }
	 	//window.location.href=window.location.href='http://localhost/Game_404_test2/Programação/game_404.html'; 
	    /*setTimeout(function () {
        window.location.href = 'http://localhost/Game_404_test2/Programação/game_404.html'; 
  		  }, 5000);*/ 
	// },
    startGame: function() {
    	window.location.href=window.location.href='http://localhost/Game-404/Programação/game_404.html'; 
		/*setTimeout(function () {
		   window.location.href = 'http://localhost/Game_404_test2/Programação/game_404.html'; 
 		  }, 20000);*/
    }	 
};